<?php
/**
 * Created by PhpStorm.
 * User: Nyemo
 * Date: 23/12/2018
 * Time: 22:28
 */
namespace api;


use Operations\CarBrandOperation;


require_once 'ApiHeader.php';

$carbrandOperation = new CarBrandOperation($manager);
$operationResult = $carbrandOperation->process();
echo json_encode($operationResult);
?>